module.exports = {
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            'vue-style-loader',
            'css-loader',
            'sass-loader'
          ]
        },
        { test: /\.html$/, use: 'vue-template-loader' }
      ],
      lintOnSave: false
    },
}