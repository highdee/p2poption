import login from '../components/public/pages/login'
import home from '../components/public/pages/home';
import reviews from '../components/public/pages/reviews';
import register from '../components/public/pages/register';
import forget from '../components/public/pages/forgot-password';
import reset_password from '../components/public/pages/password-reset';
import layout from '../components/public/layout/layout';
import about_us from '../components/public/pages/about-us';
import contact_us from '../components/public/pages/contact-us';
import verify from '../components/public/pages/verify-email';
import privacy_policy from '../components/public/pages/privacy-policy';
import LoginWithToken from '../components/public/login-with-token';

var routes = [
    {
        path: '/',
        // name: 'home',
        component: layout,
        children: [
            {
                path: '',
                name: 'home',
                component: home,
            },
            {
                path: 'review',
                name: 'review',
                component: reviews,
            },
            {
                path: 'about-us',
                name: 'about-us',
                component: about_us
            },
            {
                path: 'contact-us',
                name: 'contact-us',
                component: contact_us
            },
            {
                path:'/verify-account',
                component:verify,
            },
            {
                path:'/reset-password/:token',
                component:reset_password,
            },
            {
                path: 'login/:token',
                name: 'tokenlogin',
                component: LoginWithToken,
            },
            {
                path: '/login',
                name: 'login',
                component: login
            },
            {
                path: '/register',
                name: 'register',
                component: register
            },
            {
                path: '/forgot-password',
                name: 'forgot-password',
                component: forget,
            },
            {
                path: 'privacy-policy',
                name: 'privacy-policy',
                component: privacy_policy
            },

        ]
    },

]


export default routes;