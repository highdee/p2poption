
//Finance components
import finance from './../components/dashboard/finance/Index'
import deposit from './../components/dashboard/finance/Deposit'
import paymentAmount from './../components/dashboard/finance/inner-pages/Payment-amount' 
import withdrawal from './../components/dashboard/finance/Withdrawal'
import history from './../components/dashboard/finance/History'
import cashback from './../components/dashboard/finance/Cashback'
import promocodes from './../components/dashboard/finance/Promo_codes'
//Profile components
import trading from './../components/dashboard/profile/Index'
import trading_profile from './../components/dashboard/profile/Trading_profile'
import trading_history from './../components/dashboard/profile/Trading_history'
import security from './../components/dashboard/profile/Security'
import profile from './../components/dashboard/profile/Profile'


export default [
    //FINANCE ROUTES
    {
        path: 'finance',
        component: finance,
        children: [
            {
                path: 'deposit',
                component: deposit,
                name: 'deposit',
            }, 
            { path: 'method/:method', component: paymentAmount, name: 'paymentAmount' },
            { path: 'withdrawal', component: withdrawal, name: 'withdrawal' },
            { path: 'history', component: history, name: 'history' },
            { path: 'cashback', component: cashback, name: 'cashback' },
            { path: 'promocodes', component: promocodes, name: 'promocodes' },

        ]
    },
    // PROFILE ROUTES
    {
        path: 'profile',
        component: trading,
        children: [
            { path: 'trading-profile', component: trading_profile, name: 'trading-profile' },
            { path: 'trading-history', component: trading_history, name: 'trading-history' },
            { path: 'security', component: security, name: 'security' },
            { path: 'details', component: profile, name: 'details' },
        ]
    },

]