import login from '../components/affiliate/public/login'
import register from '../components/affiliate/public/register'
import reset_password from '../components/affiliate/public/reset-password'
import recover_password from '../components/affiliate/public/recover-password'
import layout from '../components/affiliate/dashboard/layout/app'
import index from '../components/affiliate/dashboard/index'
import profile from '../components/affiliate/dashboard/profile'
import affiliate_link from '../components/affiliate/dashboard/affiliate-link'
import public_layout from '../components/affiliate/public/layout/layout'

export default [
    {
        path:'/',
        component:public_layout,
        children: [
            {
                path: '',
                component: login,
                name: 'affiliate_login'
            },
            {
                path: 'forgot-password',
                component: reset_password,
                name: 'reset_password'
            },
            {
                path: 'recover-password/:token',
                component: recover_password,
                name: 'recover_password'
            },
            {
                path: 'register',
                component: register,
                name: 'affiliate_register'
            },
        ]
    },
    
    {
        path: '/',
        component: layout,
        children: [
            {
                path: 'dashboard',
                component: index,
                name: 'affiliate_dashboard',
            },
            {
                path: 'profile',
                component: profile,
                name: 'affiliate_profile'
            },
            {
                path: 'affiliate-link',
                component: affiliate_link,
                name: 'affiliate_link'
            },
        ],
        meta: {
            AuthRequired: true,
            affiliateAuth: true
        }
    },
]