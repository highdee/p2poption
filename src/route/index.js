import layout from './../components/dashboard/layout/dashboard-layout';
import dashboard from './../components/dashboard/index'
import tradePage from './../components/dashboard/trading/trade'
import trade from './../components/dashboard/trading/layout/app'
import public_pages from './public';
import dashboardRoutes from './dashboard';
import affiliate_index from '@/components/affiliate/index'
import affiliate from './affiliate';

import Router from 'vue-router';
import Vue from 'vue';
import store from './../store/index.js';

Vue.use(Router);

var routes = [
    {
        path: '/dashboard',
        component: layout,
        children: [
            { path: '/dashboard', component: dashboard, name: 'dashboard' },
            {
                path: '',
                component: trade,
                children: [
                    { path: 'trade-room', component: tradePage, name: 'trade_room', meta: { trade_page: true } },
                ], 
            },
            ...dashboardRoutes
        ],
        meta: {
            AuthRequired: true
        }
    }, 
    {
        path : "/demo-trade",
        redirect : {name:'trade_room'}
    },
    {
        path: '/affiliate',
        component: affiliate_index,
        children: [
            ...affiliate
        ]
    },

    {
        path:'/referral/:referral_id',
        beforeEnter: (to,from,next) => {
            localStorage.setItem('referral_id', encodeURIComponent(JSON.stringify(to.params.referral_id)));
            store.dispatch('increaseVisitor', to.params.referral_id)
            next('/register')
        }
    }
];

routes = routes.concat(public_pages);

var router = new Router({
    mode: 'history',
    routes,
});

export default router;

router.beforeEach((to,from,next)=>{
    if(to.matched.some(record => record.meta.AuthRequired)) {
        if (localStorage.getItem('p2p') == null) {
            if(to.name != 'trade_room'){
                next({
                    path: '/login',
                    name:'login'
                })
            }
        }
        else{
            next();
        }
    }

    next();
})
