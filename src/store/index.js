import Vue from 'vue';
import Vuex from 'vuex';

import mutations from './mutations';
import actions from './actions';
import getters from './getters';


Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        endpoint: process.env.VUE_APP_ENDPOINT,
        binance_api: "https://api.binance.com/api",
        notification: {
            type: 0,
            message: ''
        },
        toaster_notification_instances:{},
        chats: null,
        status: false,
        loading: false, //To show loading 
        user: null,
        token: null,
        language: '',
        max_investment_possible:2000,
        max_demo_amount_possible:20000,
        close: 0.000,
        active_symbol: {},
        asset_types:[],
        symbols: [],
        socket_connection: null,
        socket_connected: false,
        loading_chart: false,
        chart_ready: false,
        loading_symbols: false,
        loading_new_currency: false,
        subscribed: false,
        chart_data_set: [],
        chart_widget: null,
        investment_amount: 10,
        localEmitter: null,
        active_account_type: 'demo_wallet',
        selected_trade_time:0,
        purchase_times:{},
        purchase_times_ms:{},
        strike_prices:{},
        trade_data: {
            amount: 0,
            strike_price: 0,
            profit: 0,
            active: false,
            state: 'waiting',
        },
        frames:[],
        frame_id:0,
        trade_completed_data:{},
        display_orderlines:{},
        trades: [],
        completed_trades: [],
        trades_lines:{},
        strike_data: {},
        strikes_data: {},
        available_trade_times:[],
        profit_perc: 0,
        asset: 'BTC/USDT',
        openOrderPanel: false,
        openOrderBox:false,
        openHistoryPanel: false,
        openChatPanel: false,
        histories:[],
        openTimerPanel: false,
        chart_type:null,
        isTrading:false,
        openMarketModal:false,
        showTimer:false
    },
    getters,
    mutations,
    actions
});

export default store;