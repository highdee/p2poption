import axios from 'axios';

async function getInitialData(symbol) {
    var response = await axios.get("https://api.binance.com/api/v3/klines?symbol=" + symbol + "&interval=1m");
    if (response.status == 200) {
        var data = response.data;
        var newQuotes = [];
        for (var i = 0; i < data.length; i++) {
            var newQuote = {};
            newQuote.DT = formatDate(new Date(data[i][0])); // DT is a string in ISO format, make it a Date instance
            newQuote.Open = parseFloat(data[i][1]);
            newQuote.High = parseFloat(data[i][2]);
            newQuote.Low = parseFloat(data[i][3]);
            newQuote.Close = parseFloat(data[i][4]);
            newQuote.Volume = parseFloat(data[i][5]);
            newQuotes.push(newQuote);
        }
        return newQuotes;
    } else {
        return [];
    }
}
function formatDate(date) {
    date = new Date(date);
    return date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
}
function convertToNextMinuteTime(time) {
    var date = new Date(time + (60*1000));
    date = Date.UTC(date.getFullYear(), date.getUTCMonth(), date.getUTCDate(), (date.getUTCHours()), date.getMinutes(),0);
    return date;
}

function drawArrow(context,time,price){
    context.state.chart_widget.activeChart().createShape(
        { time:  Math.round(new Date(convertToNextMinuteTime(time)) / 1000), price: price },
        {
            shape: "arrow_up",
            lock: true,
            disableSelection: true,
            disableSave: true,
            disableUndo: true
        }
    );
}

function frameDisplayText(context,time,price,text){
    context.state.chart_widget.activeChart().createShape(
        { time:  Math.round(new Date(convertToNextMinuteTime(time)) / 1000), price: price },
        {
            shape: 'text',
            lock: false,
            zOrder: 'top', 
            disableSelection: true,
            disableSave: true,
            disableUndo: true,
            text: text,
            overrides: {
                fontsize:11,
                color: '#ffffff',
                bold:true,
                linewidth: 2
            }
        }
    );
}

function setSymbolCallback(context,data){
    var displays = {...context.state.trade_completed_data};
    // If frame has some completed trades that has not been displayed
    // then we need to change visible range back to the trade time
    // and also draw necessary shapes
    if(displays[data.frame_id]){
        var frame_display_trades = displays[data.frame_id];
        //get one of the trades so we can know the start and finish trade time of the frame since they will all close at once 
        // (each frame has its trading time where all trade in that frame closes)
        var sample_trade = frame_display_trades[0];
        var from = sample_trade.purchase_time_ms;
        var to = sample_trade.purchase_time_ms + (sample_trade.total_active_session_seconds * 1000);

        
        console.log('Done')


        try {
            context.state.chart_widget
            .activeChart()
            .setVisibleRange({from: from, to: to}, {percentRightMargin: 20})
            .then(() => console.log('New visible range is applied'));

            var trades = frame_display_trades;
            var display_ref ={};
            
            trades.forEach(order => {
                display_ref['d_'+order.id] = makeTradeLine(context,order.amount, order.direction, order.strike_price);

                // drawArrow(context, order.end_data.timestamp, order.strike_price);
                // frameDisplayText(context, order.end_data.timestamp, order.close_price,'Result is '+order.profit);

            });
            context.state.display_orderlines = {...display_ref};
        } catch (e) {
            ///
            console.log('setsymbolcallback error ',e)
        }
    }else{
    // THE DEFAULT CALLBACK SHOULD BE CALLED SO WE CAN RESET CHART TO DEFAULT VISIBLE RANGE
        if(data.callback){
            data.callback();
        }
    }
}

function makeTradeLine(context,capital, direction, price) {
    var color = direction == 'up' ? '#27a69a' : '#ff0000';
    return context.state.chart_widget
        .activeChart()
        // .createOrderLine()
        .createPositionLine()
        .setText('')
        .setPrice(price)
        .setQuantity('$' + capital)
        .setLineWidth(2)
        .setLineColor(color)
        .setLineStyle(0)
        .setQuantityBackgroundColor(color)
        .setQuantityTextColor('#ffffff')
        .setQuantityBorderColor(color)
        .setBodyBorderColor(color);
}


export default {
    setSymbol(context, data) {
        var symbols = context.state.asset_types.find((d) => d.title == data.type).assets;
        context.state.symbols = symbols;
        var symbol = context.state.symbols.find((d) => d.name == data.name);
        var thesameSymbol = false;

        if(symbol){
            if(context.state.active_symbol.name == symbol.name){
                thesameSymbol=true
            }

            //  SET SYMBOL
            symbol.symbol = symbol.base_asset + symbol.quote_asset;
            context.state.active_symbol = { ...symbol };

            //Adding new frame
            var temp ={...symbol};
            //Incase we really need to create a new frame (for example if user refresh and we need to use the on-going trade to create a frame for user)
            //This is still limited if the a frame_id that was passed already exit then no new frame will be created
            temp.forceCreate = data.forceCreate;

            // Setting the trade time of the frame if it was passed along
            if(data.trade_time) temp.trade_time = data.trade_time;

            if((data.frame_id == undefined || data.create_frame)){
                context.dispatch('addToNavAssets',temp);
            }else{
                // FRAME ALREADY EXIT
                context.state.frame_id = data.frame_id;

                console.log('changing frame time');
                // Frame Timing
                context.dispatch('changeFrameTime',data.frame_id);
            }

            
            if(context.state.chart_widget && context.state.chart_ready){
                // set new symbol for chat
                context.state.chart_widget.activeChart().setSymbol(data.name,function(){
                    setSymbolCallback(context,data);
                });

                // this is called to help the vertical line display corrrectly (TradingView wahala)
                if(thesameSymbol){
                    setTimeout(()=>{
                        setSymbolCallback(context,data);
                    },400);
                }
            }
        }
    },
    tradeCompleted(context,data){
        setSymbolCallback(context,data);
    },
    changeFrameTime(context,id){ 
        var frame = context.state.frames.find((d)=> d.id == id);
        if(!frame) return false

        // Set frame time
        if(frame.trade_time != null){
            var time = {...frame.trade_time};
            context.state.selected_trade_time=parseInt(time.name);
            context.state.profit_perc=parseFloat(time.pivot.percentage); 
            console.log(context.state.profit_perc);
        }else{
            if(context.state.active_symbol.timing.length > 0){
                var timing =context.state.active_symbol.timing[0];
                context.state.profit_perc = timing.pivot.percentage;
                context.state.selected_trade_time = timing.name;
                console.log('rerer',timing);
            }
        }
    },
    removeFrame(context,data){
        var frames = [...context.state.frames];
        if(frames.length  == 1) return false;

        var id = data.id;
        var next_frame_id = context.state.frame_id;
        var ids = [...frames.map((d)=> d.id)];
        var position = ids.indexOf(id);
        var frame = frames.find((d)=> d.id == id);
        if(!frame) return false

        // Trade is active and can not be quit
        if(frame.state == 'trading-on' || frame.state == 'waiting') {
            context.dispatch('dangerNotify',{header:'',body:"You can't quite this frame at this time",context:data.context})
            return false;
        }

        // Frame is the active frame
        if(frame.id == context.state.frame_id){
            if(position == 0){next_frame_id = frames[position+1].id}
            else{ next_frame_id = frames[position-1].id }
        }

        frames = frames.filter((d)=> d.id != id);
        context.state.frames = [...frames];
        

        var nextframe = frames.find((d)=> d.id == next_frame_id);
        nextframe.asset.frame_id = next_frame_id;
        context.dispatch('setSymbol',nextframe.asset);
    },
    updateFrameStatus(context,data){
        var frames = context.state.frames;
        frames = frames.map((d)=>{
            if(d.id == data.id){
                d['state'] = data.value;
            }
            return d;
        });
        context.state.frames = frames;
    },
    updateFrameAttribute(context,data){
        var frames = context.state.frames;
        frames = frames.map((d)=>{
            if(d.id == data.id){
                d['attribute'] = data.value;
            }
            return d;
        });
        context.state.frames = frames;
    },
    updateFrameTradeTime(context,data){
        var frames = context.state.frames;
        frames = frames.map((d)=>{
            if(d.id == data.id){
                d['trade_time'] = data.value;
            }
            return d;
        });
        context.state.frames = frames;
    },
    addToNavAssets(context,symbol){
        var frame = {};
        var frames = [...context.state.frames];

        // checking if the asset already has a frame
        var exist = frames.find((d)=> d.asset.symbol == symbol.symbol);
        if(exist){
            // FRAME ALREADY EXIT
            if(!symbol.forceCreate){
                context.state.frame_id = exist.id;
                return false;
            }

            // If force create is coming from 'when a user selected a trade time' then before accepting the forceCreate request we need to be sure
            // there is no frame with thesame symbol and trade_time
            if(symbol.trade_time){
                exist = frames.find((d)=> d.asset.symbol == symbol.symbol && d.trade_time == symbol.trade_time);
                if(exist){
                    context.state.frame_id = exist.id;
                    return false;
                }
            }
            
        }

        frame.id = symbol.frame_id ? symbol.frame_id : Math.ceil(Math.random() * 99999);
        frame.state = 'none';
        frame.attribute = 'ready';
        frame.asset = symbol;
        frame.trade_time = symbol.trade_time ? symbol.trade_time : null;

        // IF FRAME NEVER EXIT
        if(frames.find((d)=> d.id == frame.id) == undefined){
            frames.push(frame);
            context.state.frames = [...frames];
            context.state.frame_id = parseInt(frame.id);

            // Frame Timing
            context.dispatch('changeFrameTime',frame.id);
        } 
        console.log(frames);
    },
    selectSymbol(context, symbol) {
        context.state.active_symbol = symbol;
        context.dispatch('subscribeToSymbol', symbol);
    },
    resetModals(context){
        context.state.openOrderPanel = false;
        context.state.openOrderBox = false;
        context.state.openHistoryPanel = false;
        context.state.openChatPanel = false;
        context.state.openTimerPanel = false;
        context.state.openMarketModal = false;
        context.state.openChartTypeModal = false;
        context.state.showTimer  = false;
    },
    resetNoneTradeHistoryModal(context){
        // context.state.openOrderPanel = false;
        context.state.openOrderBox = false;
        context.state.openChatPanel = false;
        context.state.openTimerPanel = false;
        context.state.openMarketModal = false;
        context.state.openChartTypeModal = false;
    },
    subscribeToSymbol(context, symbol) {
        getInitialData(symbol.symbol)
            .then((data) => {
                if (!context.stxx) { return false; }
                context.state.socket_connection.send(JSON.stringify({
                    "id": 1,
                    "method": "SUBSCRIBE",
                    "params": [symbol.symbol.toLowerCase() + '@kline_1m'],
                }));
                var temp = JSON.parse(JSON.stringify(data));
                context.state.chart_data_set = temp;
                setTimeout(() => {
                    context.state.stxx.updateChartData(temp);
                    context.state.stxx.updateChartData(temp[temp.length - 1]);
                    context.state.stxx.streamParameters.maxTicks = 0;
                    context.state.subscribed = true;
                }, 0);
            });
    },
    unSubscribeFromSymbol(context, symbol) {
        context.state.socket_connection.send(JSON.stringify({
            "id": 2,
            "method": "UNSUBSCRIBE",
            "params": [symbol.symbol.toLowerCase() + '@kline_1m'],
        }));
        context.state.subscribed = false;
    },
    successNotify(context, state) {
        state.context.$notify({
            group: 'foo',
            type: 'success',
            position: 'bottom',
            duration: 3000,
            closeOnClick: true,
            title: state.header,
            text: state.body
        });
    },
    dangerNotify(context, state) {
        state.context.$notify({
            group: 'foo',
            type: 'error',
            position: 'bottom',
            duration: 3000,
            closeOnClick: true,
            title: state.header,
            text: state.body
        });
    },
    handleError(context, error) {
        if (error.request.status == 422) {
            var resp = JSON.parse(error.request.response);
            var err = resp.errors;
            var msg = '';
            for (var item in err) {
                msg = err[item][0];
                break; // it picks the first error ;
            }
            context.commit('setNotification', { type: 2, msg: msg });
            return msg;
        } else if (error.request.status == 303) {
            resp = JSON.parse(error.request.response);
            context.commit('setNotification', { type: 2, msg: resp.error });
        } else if (error.request.status == 404) {
            resp = JSON.parse(error.request.response);
            msg = 'Request not found';
            context.commit('setNotification', { type: 2, msg: msg });
        } else if (error.request.status == 400) {
            resp = JSON.parse(error.request.response);
            msg = resp.msg;
            context.commit('setNotification', { type: 2, msg: msg });
        } else if (error.request.status == 401) {
            msg = 'Oops! Authentication error, Please login again';
            context.commit('setNotification', { type: 2, msg: msg });
            // context.commit('logout');
        } else {
            msg = 'Oops! server error, Please try again';
            context.commit('setNotification', { type: 2, msg: msg });
        }
    },
    post(context, data) {
        return new Promise((resolve, reject) => {
            if(data.endpoint.indexOf('http') == -1) { 
                data.endpoint = context.state.endpoint + data.endpoint;
            }
            axios
                .post(data.endpoint, data.details, {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token,
                    },
                })
                .then((data) => {
                    resolve(data);
                })
                .catch((error) => {
                    context.dispatch('handleError', error);
                    reject(error);
                });
        });
    },
    get(context, endpoint) { 
        return new Promise((resolve, reject) => {
            if(endpoint.indexOf('http') == -1) { 
                endpoint = context.state.endpoint + endpoint;
            }
            axios
                .get(endpoint, {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token,
                    },
                })
                .then((data) => {
                    resolve(data);
                })
                .catch((error) => {
                    console.log('error', error.request);
                    context.dispatch('handleError', error);
                    reject(error);
                });
        });
    },
    loginUser(context, data) {
        context.commit('setLoading', true);
        var ua = window.bowser.getParser(window.navigator.userAgent);
        console.log(ua);
        var user_device_info = {
            'browser': ua.parsedResult.browser.name,
            'device': ua.parsedResult.os.name,
            'device_type': ua.parsedResult.platform.type
        };
        data.detail = {
            ...data.detail,
            ...user_device_info
        };

        return new Promise((resolve, reject) => {
            axios
                .post(context.state.endpoint + data.endpoint, data.detail)
                .then((res) => {
                    if (res.data.status) {
                        context.commit('setUser', res.data);
                        setTimeout(() => {
                            window.location.href = '/dashboard/trade-room';
                        }, 500);
                        resolve();
                    } else {
                        context.commit('setNotification', {
                            type: 2,
                            msg: res.data.msg,
                        });
                    }
                    context.commit('setLoading', false);
                })
                .catch((error) => {
                    console.log(error);
                    context.dispatch('handleError', error);
                    reject(error);
                    context.commit('setLoading', false);
                });
        });
    },
    createAccount(context, data) {
        return new Promise((resolve, reject) => {
            var referral_id = localStorage.getItem('referral_id');
            referral_id = decodeURIComponent(referral_id);
            referral_id = JSON.parse(referral_id);
            context.commit('setLoading', true);
            axios
                .post(context.state.endpoint + '/create-account?referral_id=' + referral_id, data)
                .then((res) => {
                    context.commit('setLoading', false);
                    console.log(res);
                    if (res.data.status) {
                        context.commit('setUser', res.data);
                        window.localStorage.removeItem('referral_id');
                        window.location.href = '/dashboard/profile/details';
                        resolve();
                    } else {
                        context.commit('setNotification', {
                            type: 2,
                            msg: res.data.msg,
                        });
                    }
                })
                .catch((error) => {
                    context.commit('setLoading', false);
                    if (data.socialSignIn) {
                        if (error.request.status == 422) {
                            context.dispatch('loginUser', { detail: data, endpoint: '/login' });
                        }
                    } else if (error.request != null) {
                        context.dispatch('handleError', error);
                        reject(error);
                    }
                });
        });
    },
    getDashboard(context) {
        context
            .dispatch('get', '/get-dashboard')
            .then(() => {
                // console.log(data)
                // if (data.data.status) {
                //     context.commit('setUser', { user: data.data.data, token: context.state.token });
                // } else {
                //     context.commit('setNotification', { type: 2, msg: data.data.msg });
                //     context.commit('logout');
                // }
            })
            .catch((error) => {
                context.dispatch('handleError', error);
            });
    },
    getAffiliateDashboard(context) {
        context.dispatch('get', '/affiliate/get-affiliate-dashboard')
            .then((data) => {
                if (data.data.status) {
                    var aff = data.data.data
                    aff.affiliate = true
                    aff.referral = data.data.referral
                    // console.log(aff)
                    context.commit('setUser', { user: aff, token: context.state.token });
                } else {
                    context.commit('setNotification', { type: 2, msg: data.data.msg });
                    context.commit('Affiliatelogout');
                }

            })
    },
    increaseVisitor(context, referral) {
        context.dispatch('get', '/affiliate/increase-visitor?referral=' + referral)
    },
    logout(context) {
        context.dispatch('post', { endpoint: '/logout', details: {} });
        context.commit('logout');
    }
};
