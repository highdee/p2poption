export default {
    setChartInfo(state, data) {
        state.close = data.close ?? state.close;
    },

    setNotification(state, data) {
        state.notification.type = 0;
        state.notification.message = '';


        setTimeout(() => {
            state.notification.type = data.type;
            state.notification.message = data.msg;
            // if (data.unset) {
            setTimeout(() => {
                state.notification.type = 0;
                state.notification.message = '';
            }, 6000);
            // }
        }, 200);
    },

    setLoading(state, status) {
        state.loading = status
    },

    getUser(state) {
        var data = localStorage.getItem('p2p');
        data = decodeURIComponent(data);
        data = JSON.parse(data);

        state.user = data.user;
        state.token = data.token;

        if (data.account_type) {
            state.active_account_type = data.account_type;
        }

        var result = encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('p2p', result);


    },

    setUser(state, data) {
        state.user = { ...state.user, ...data.user };
        state.token = data.token ?? state.token;

        if (!data.account_type && state.account_type) {
            data.account_type = state.account_type;
        } else {
            state.account_type = data.account_type;
        }

        var result = encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('p2p', result);
    },

    setChat(state, data){
        state.chats = data
    },

    logout(state) {
        window.localStorage.removeItem('p2p');
        state.user = {};
        state.token = null;
        window.location.href = "/login";
    },

    Affiliatelogout(state) {
        window.localStorage.removeItem('p2p');
        state.user = {};
        state.token = null;
        window.location.href = '/affiliate';
    },
}