export default {
    methods: {
        attempLeft(user){
            var attempted = user.no_of_trade;
            var attempt_left = 10 - attempted;
            return attempt_left >= 0 ? attempt_left : 0;
        },
        formatCurrency(amount){
            if(amount.toString().length <= 3 ) return amount;
            var comma_spot_count = 0;
            var amount_in_string = amount.toString();
            var result = '';
            var currency_char ='';
            for(var count = (amount_in_string.length - 1); count >= 0; count--){
                currency_char = amount_in_string[count];
                result = currency_char + result;
                comma_spot_count += 1;
                if(comma_spot_count == 3 && count > 0){
                    comma_spot_count = 0;
                    result = ','+result;
                }
            }
            return result;
        }
    },
}