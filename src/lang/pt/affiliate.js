export default{
    "banner_text_1": "Mais",
    "banner_text_2": "ofertas lucrativas",
    "banner_text_3": "para afiliados",
    "banner_title_login_1": "Profit sharing commission is up to 80%",
    "banner_text_login_1": "Atualize o status do seu perfil para aumentar sua comissão de participação nos lucros de 50% para 80% como IB!",
    "banner_title_login_2": "Profit sharing commission is up to 80%",
    "banner_text_login_2": "Atualize o status do seu perfil para aumentar sua comissão de participação nos lucros de 50% para 80% como IB!",
    "banner_title_login_3": "Profit sharing commission is up to 80%",
    "banner_text_login_3": "Atualize o status do seu perfil para aumentar sua comissão de participação nos lucros de 50% para 80% como IB!",
    "login": "Faça login",
    "register": "Faça login",
    "login_fb": "Facebook",
    "login_g": "Google",
    "login_or": "ou",
    "login_fp": "Lembrar de mim",
    "login_rem": "Esqueci a senha",
    "sign_in_btn": "Entrar",
    "authorizing": "Autorizando",
    "not_yet": "Ainda não registrado",
    "sign_up_alt": "Inscrever-se",
    "toc": "Li e aceito o contrato de afiliado",
    "sign_up_btn": "inscrever-se",
    "sign_up_btn_loading": "carregando",
    "sign_up_alt_log": "Já registrado",
    "sign_up_alt_btn": "Login",
    "reset_password": "Esqueci a senha",
    "resetting_password": "Redefinir senha",
    "resetting_password_btn": "Redefinir senha",
    "resetting_password_btn_loading": "Redefinir senha",
    "reset_password_btn": "Redefinir senha",
    "reset_password_btn_loading": "Sending Email",
    "Campaign_name": "Campaign name",
    "Promo_code": "Promo code",
    "Offer_type": "Offer type",
    "Cashback": "Cashback",
    "text4": "Do not use promo code",

    // affiliate header
    "Visitors": "visitantes",
    "Reg": "Reg",
    "Commission": "Comissão",
    "Balance": "Saldo",
    "refferal_link": "link de referência",

    // App
    "Dashboard": "Painel",
    "My_affiliate_links": "Meus links de afiliados",
    "Profile": "Perfil",
    "Logout": "Sair",

    // Login
    "affiliates_header": "Ofertas mais lucrativas para afiliados",
    "affiliate_hero_text1": "Participação nos lucros! Sistema avançado de participação nos lucros com renda passiva de longo prazo.",
    "affiliate_hero_text2": "Bônus para FTDs! Sistema de bônus adicional com pagamentos para os primeiros depósitos dos clientes.",
    "affiliate_hero_text3": "Pagamentos semanais! - Quanto mais alto o status do seu Perfil, mais pagamentos frequentes você pode ter.",
    "affiliate_hero_text4": "Comissão de rotatividade líquida! Você recebe comissão por qualquer aposta. Quanto maior o volume de negociação, mais comissão você terá.",
    "affiliate_hero_text5": "Concursos de afiliados com participação gratuita. Obtenha comissão adicional e ganhe prêmios incríveis pelo seu trabalho.",
    "Sign_in": "Entrar",
    "E-mail": "E-mail",
    "Password": "Senha",
    "Forgot_your_password": "Esqueceu sua senha?",
    "Not_yet_registered": "Ainda não está registrado?",
    "Authorizing": "Autorizando",
    "footer_h5": "Profit sharing commission is up to 80%",
    "footer_p1": "Atualize seu status de perfil para aumentar sua comissão de participação nos lucros de 50% até 80% como IB!",
    "footer_p2": "Atraia novos clientes e obtenha bônus adicionais!",
    "footer_p3": "Quanto mais clientes com FTDs você atrair, maior será o valor do bônus que você get!",
    "footer_p4": "Ganhe dinheiro com a receita líquida de seus indicados!",
    "footer_p5": "Receba comissão de cada aposta de referência. Quando a empresa tem lucro, você também ganha uma receita adicional de compartilhamento de comissão.",

    // Register
    "Confirm_Password": "Confirme a senha",
    "I_accepted": "Li e aceito o",
    "affiliate_agreement": "acordo de afiliado",
    "Already_registered": "Já registrado?",
    "Sign_up": "Inscreva-se",
    "Registering": "Registrando-se",

    // Reset Password
    "Reset_Password": "Redefinir senha",
    "Password_Reset_Link": "Enviar um link de redefinição de senha",
    "Sending_email": "Enviando e-mail",
    "Go_back": "Voltar",

    // Recover Password
    "New_Password": "Nova senha",
    "Enter_new_password": "Digite a nova senha",
    "Re-enter_password": "Digite a senha novamente",
    "Resseting_Password": "Ressetando a senha",
    "Smart_link": "Link inteligente",
    "text1": "Este é um link padrão criado para obter a melhor conversão em registros e depósitos",
    "text2": "O link é gerado de forma a obter os melhores resultados na conversão do tráfego. Seu tráfego será redirecionado para as páginas do site que garantem a maior receita.",
    "Start": "INICIAR",
    "bonus": "bônus",
    "min_deposit": "depósito mínimo",
    "MORE_CHANCE": "MAIS CHANCE",
    "Volume_sharing": "Compartilhamento de volume",
    "Turnover_sharing": "Compartilhamento de volume de negócios",
    "Deposit_sharing": "Compartilhamento de depósito",
    "Revenue_sharing": "Compartilhamento de receita",
    "CPA": "CPA",
    "text5": "Cashback - compartilhe uma parte de sua comissão com seus indicados. É calculado individualmente para cada referência com base no volume de negociação líquido para o período de liquidação.",
    "Create": "Criar",
    "creating": "criar",
    "Campaigns": "Campanhas",
    "Campaign_Name": "Nome da campanha",
    "Register_user": "Registrar usuário",
    "CTR": "CTR",
    "RTD": "RTD",
    "Link": "Link",
    "Delete": "Excluir",
    "deleting": "excluindo",
    "Find": "Encontrar",
    "Yesterday": "Ontem",
    "Current_week": "Semana atual",
    "Current_month": "Mês atual",
    "Last_week": "Semana passada",
    "Last_month": "Mês passado",
    "Custom_settings": "Configurações personalizadas",
    "Date_from": "Data de",
    "Date_to": "Data para",
    "Referral_link": "Link de referência",
    "text6": "Sua conta de afiliado foi banida, por favor, contate nosso provedor de serviços para ativar",

    // profile
    "Personal_information": "Informações pessoais",
    "Save": "Salvar",
    "Saving": "Salvar",
    "smart_link": "Links inteligentes",
    "smart_link_text": "Este é um link padrão criado para obter a melhor conversão em cadastros e depósitos",
    "smart_link_paragraph": "O link é gerado de forma a obter os melhores resultados na conversão de tráfego. Seu tráfego será redirecionado para as páginas do site que garantem a maior receita",     
    "create_new_link": "Criar novo link de afiliado",
    "create_btn": "criar",
    "create_btn_loading": "criar",
    "campaign": "campanha",
    "today": "hoje",

    // Campaign table
    "campaign_name": "Nome da campanha",
    "offer_type": "Tipo de oferta",
    "promo_code": "Código promocional",
    "register_user": "Registrar usuário",
    "personal_info": "Informações pessoais",
    "first_name": "nome",
    "last_name": "sobrenome",
    "email": "e-mail",
    "username": "nome de usuário",
    "phone": "nome de usuário",
    "country": "País",
    "profile_btn": "salvar",
    "profile_btn_loading": "Atualizando"
}