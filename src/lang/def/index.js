import homepage from './homepage';
import dashboard from './dashboard';
import affiliate from './affiliate'

export default {
    home: homepage,
    dashboard,
    affiliate,
}
