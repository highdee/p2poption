export default {
    // Finance folder..... 
    // Deposit
    "panel_title": "Wählen Sie ein Zahlungssystem für eine Investition",
    "crypto_currency": "Kryptowährung",
    "ethereum": "Ethereum",
    "USD_tether": "USD Tether",
    "cards": "Karten",
    "bitcoin": "Bitcoin",

    // History
    "history_title": "Kontostandverlauf",
    "filter": "Filter",
    "apply": "ANWENDEN",
    "applyy": "Anwenden",
    "balance_history": "Kontostandverlauf",
    "deposits": "Einzahlungen",
    "withdrawals": "Auszahlungen",
    "order": "Bestellung",
    "date": "Datum",
    "amount": "Betrag",
    "method": "Methode",
    "type": "Typ",
    "status": "Status",
    "bonus_amount": "Bonusbetrag",
    "nothing_found": "Nichts gefunden",
    "export_to_xls": "Export nach XLS",
    "trading_history_title": "Trading History",

    "to": "to",

    "deposit": "deposit",
    "Withdrawal": "withdrawal",
    "history": "history",
    "cashback": "cashback",
    "promo_codes": "promo codes",
       
    // Promo Codes
    "promo_Codes": "Promo-Codes",
    "promocode_text1": "Haben Sie einen Promocode?",
    "promocode_text2": "Verfügbare Promo-Codes",
    "percent": "Prozent",
    "min_deposit": "Min. Einzahlung",
    "max_bonus": "Maximaler Bonus",
    "valid_from": "Gültig ab",
    "valid_until": "Gültig bis",
    "date_from": "Datum ab",
    "date_to": "Datum bis",
    "withdrawal": "Auszahlung",
    "free_balance": "Verfügbares Guthaben:",
    "USD": "USD",
    "withdrawal_amount": "Mindestauszahlungsbetrag",
    "commission": "Brieftaschen-ID:",
    "message_withdrawal_text1": "Sie können Geld von Ihrem Guthaben auf Ihre Bankkarte oder Geldbörse abheben, die Sie für die Einzahlung verwendet haben.",
    "message_withdrawal_text2": "Sie können jederzeit eine Auszahlung beantragen. Ihre Auszahlungsanfragen werden innerhalb von 3 Werktagen bearbeitet.",
    "check": "Check",

    // Cashback
    "your_cashback": "Ihr Cashback",
    "last_payout": "Letzte Auszahlung",
    "total_cashback": "Gesamt-Cashback",
    "max_payout": "Maximale Auszahlung",
    "next_payout": "Nächste Auszahlung",
    "date_of_activation": "Datum der Aktivierung",
    "cashback_text1": "Bei Verlusten gibt der Cashback jeden Monat einen Teil des verlorenen Geldes zurück.",
    "cashback_text2": "Sie können bis zu 10% zurückgeben von Ihren Verlusten.",
    "cashback_text3": "Sie haben im Moment kein aktives Cashback.",
    "buy_it_now": "Jetzt kaufen",
    "cashback_payouts": "Cashback-Auszahlungen",
    "cashback_percent": "Cashback-Prozentsatz",
    "balance": "Kontostand",
    "activation_history": "Aktivierungsverlauf",
    "end_date": "Enddatum",
    "terms_and_conditions": "Allgemeine Geschäftsbedingungen",
    "cashback_text4": "Cashback läuft automatisch 1 Jahr nach Aktivierung ab.",
    "cashback_text5": "Sie können Ihren Cashback jederzeit verlängern, wenn Sie Cashback mit derselben Auszahlung gekauft haben Prozentsatz.",
    "cashback_text6": "Sie können den Cashback-Prozentsatz (maximal 10%) jederzeit erhöhen, wenn Sie Cashback mit einem höheren Auszahlungsprozentsatz als dem aktuellen gekauft haben. Zum Zeitpunkt der Aktivierung wird ein neuer Cashback-Prozentsatz angewendet.",
    "cashback_text7": "Der Cashback wird berechnet, wenn der Gesamtbetrag der Verluste größer ist als die Gewinne des Vormonats oder seit dem Datum der Aktivierung. Cashback wird Ihrem Guthaben am ersten Tag eines jeden Monats automatisch gutgeschrieben.",
    "cashback_text8": "Sie können Ihr Cashback jederzeit abheben, wenn Sie über genügend Guthaben auf Ihrem Live-Kontostand verfügen.",
    "cashback_text9": "Das Unternehmen hat das Recht, die Bonusbedingungen jederzeit ohne vorherige Ankündigung zu ändern oder diese Aktion zu beenden.",

    //Payment Confirmed
    "cancel_payment": "Zahlung stornieren",
    "Deposit": "Einzahlung",
    "payment_text1": "Achtung: Intelligente Vertragszahlungen werden NICHT akzeptiert!",
    "payment_text2": "Um die Zahlung abzuschließen, überweisen Sie bitte",
    "payment_text3": "Ich habe eine Zahlung geleistet",
    "payment_text4": "Vielen Dank für die Zahlungen",
    "payment_text5": "Wir werden Ihre Brieftasche gutschreiben, sobald wir Ihre Zahlung bestätigt haben .",
    "copy_address": "Adresse kopieren",
    "copy_amount": "Betrag kopieren",
    "OKAY": "OKAY",

    // Payment Amount
    "payment_amount_text1": "Wählen Sie eine andere Zahlungsmethode",
    "payment_amount_text2": "Mindesteinzahlungsbetrag ist",
    "payment_amount_text3": "Maximaler Betrag pro Transaktion:",
    "no_limits": "keine Begrenzung",
    "payment_amount_text5": "Ungültiger Gutscheincode",
    "payment_amount_text6": "Minimale Einzahlung für den Gutscheincode:",
    "payment_amount_text7": "Verfügbar für Aktivierung (Zeiten):",
    "payment_amount_text8": "Gutscheincode löschen Feld",
    "Promo_code": "Promo-Code:",
    "Bonus": "Bonus",
    "delete": "Löschen",
    "continue": "Weiter",
    "Initialising_Payment": "Zahlung initialisieren ...",
    "You_get": "Sie erhalten",
    "bonus": "Bonus",
    "choose_your_bonus": "Wählen Sie Ihren Bonus",
    "your_payment_amount": "Ihr Zahlungsbetrag:",
    "your_bonus_amount": "Ihr Bonusbetrag:",
    "bonus_available": "Es ist kein Bonus verfügbar",
    "GET_BONUS": "ERHALTEN SIE 50% BONUS",
    "first_deposit": "bei Ihrer ersten Einzahlung",
    "DEMO": "DEMO",
    "LIVE_ACCOUNT": "LIVE-KONTO",
    "SWAP_FREE_ACCOUNT": "Tauschfreies Konto",
    "Top_Up": "Aufladen",
    "MT5_LIVE": "MT5 LIVE:",
    "MT5_Demo": "MT5-Demo:",
    "Live_account_statistics": "Live-Kontostatistik",
    "Live_statistics": "Live-Kontostatistik für heute:",
    "Trading_profit": "Handelsgewinn:",
    "Email_status": "E-Mail-Status:",
    "Identity_status": "Identitätsstatus:",
    "Unverified": "Nicht überprüft",
    "pending": "Ausstehend",
    "Address_status": "Adressstatus:",
    "Profile": "Profil",
    "News": "Nachrichten",
    "Support": "Support",
    "support": "Support",
    "Trading_turnover": "Handelsumsatz:",
    "Net_turnover": "Nettoumsatz:",
    "online": "online",
    "Settings": "Einstellungen",
    "Logout": "Abmelden",
    "Languages": "Sprachen",
    "STRANGER": "FREMDER",
    "Demo_Account": "Demokonto",
    "app_text1": "Liste der Favoriten-Assets",
    "app_text2": "Hallo, benutze diesen Chat für allgemeine Fragen ...",
    "Trading": "Handel",
    "Finance": "Finanzen",
    "Market": "Markt",
    "Achievements": "Erfolge",
    "Chat": "Support",
    "Help": "Hilfe",
    "Chats": "Chats",
    "General_questions": "Allgemeine Fragen",
    "Channels": "Kanäle",
    "Notification": "Benachrichtigung",
    "Search": "Suche",
    "Add": "Hinzufügen",
    "Quick_Trading": "Schneller Handel",
    "Real_Account": "Reales Konto",
    "Digital_Trading": "Digitaler Handel",
    "MT5_Forex": "MT5 Forex",
    "Purchases": "Käufe",
    "Lottery": "Lotterie",
    "Mining": "Bergbau",
    "Support_Service": "Support-Service",
    "Guides_and_Tutorial": "Anleitungen und Tutorial",
    "Reviews": "Rezensionen",
    "Apps": "Apps",
    "Quick_Account": "Real-Konto für den schnellen Handel",
    "Digital_Real_Trading": "Real-Konto für den digitalen Handel",
    "MT5_Forex_Account": "Real-Konto für den MT5-Forex",
    "Quick_Demo_Account": "Demo-Konto für den schnellen Handel",
    "Digital_Demo_account": "Demo-Konto für den digitalen Handel",
    "MT5_Demo_Account": "Demo-Konto für den MT5-Handel] ]",
    "History": "Verlauf",
    "Cashback": "Cashback",
    "Promo_codes": "Promo-Codes",
    "Trading_Profile": "Handelsprofil",
    "Security": "Sicherheit",
    "Trading_History": "Handelsverlauf",
    "Gems_lottery": "Edelsteinlotterie",
    "Gems_mining": "Edelsteinabbau",
    "Support_Services": "Support-Services",
    "Platform_Guides": "Plattformführer",
    "Applications": "Anwendungen",
    "Signals": "Signale",
    "Social_Trading": "Social Trading",
    "Express_Trade": "Express Trade",
    "Tournaments": "Turniere",
    "Pending_trades": "Ausstehende Trades",
    "OPENED": "GEÖFFNET",
    "CLOSED": "GESCHLOSSEN",
    "No_placed_trades": "Keine platzierten Trades",
    "No_closed_trades": "Keine geschlossenen Trades",
    "Currencies": "Währungen",
    "About_Us": "Über uns",
    "Contacts": "Kontakte",
    "Term_and_Condition": "Allgemeine Geschäftsbedingungen",
    "AML_KYC_Policy": "AML- und KYC-Richtlinie",
    "Privacy_Policy": "Datenschutzrichtlinie",
    "Payment_Policy": "Zahlungsrichtlinie",
    "Responsibility_Disclosure": "Offenlegung der Verantwortung",
    "Promo_Codes": "Promo-Codes",
    "More": "Mehr",
    // market folder
    "MARKET": "MARKET",
    "PURCHASES": "PURCHASES",
    "GEMS_LOTTERY": "GEMS LOTTERY",
    "GEMS_MINING": "GEMS MINING",
    // Gems lottery
     "Summary": "Zusammenfassung",
    "Gem": "Edelstein",
    "Quantity": "Menge",
    "Terms_and_Conditions": "Allgemeine Geschäftsbedingungen",
    "Filters": "Filter",
    "Apply": "Anwenden",
    "Reset": "Zurücksetzen",
    "Trade_amount": "Handelsbetrag",
    "Lottery_number": "Lotterienummer",

    // Gems Mining
    "Mined_out_shards": "Ausgebaute Scherben",
    "Purchase_a_licence": "Lizenz erwerben",
    "Timeframe": "Zeitrahmen",
    "General": "Allgemein",
    "Number_of_trades": "Anzahl der Trades",
    "Profitable_deals": "Profitable Deals",
    "Profit": "Profit",
    "Ruslan_V": "Ruslan V.",
    "Export_to_XLS": "Export nach XLS",

    // Purchase
    "purchase_text": "Promo-Code für Einzahlungsbonus",
    "Available": "Verfügbar",
    "No_purchases": "Keine Einkäufe",
    "Go_to_the_Market": "Zum Markt gehen",
    "Promo_Code": "Promo-Code",

    // Index
    "PROMO_CODES": "رموز ترويجية",
    "Risk_free": "بدون مخاطر",
    "Risk": "مخاطر",
    "BONUS_TO_BALANCE": "BONUS TO BALANCE",
    "boosters": "معززات",
    "prolongators": "أدوات إطالة",
    "gems": "أحجار كريمة",
    "chest": "صندوق",
    "vip_tickets": "تذاكر vip",
    "Conditions": "الشروط",
    "Cost": "Kosten",
    "trading_profile": "Handelsprofil",
    "profile": "Profil",
    "security": "Sicherheit",
    "trading_history": "Handelsgeschichte",
   
    // Profile
    "First_Name": "Vorname",
    "last_name": "Nachname",
    "verified": "Verifiziert",
    "Email": "E-Mail",
    "update": "UPDATE",
    "updating": "UPDATING",
    "Resend": "Erneut senden",
    "sending": "Senden",
    "Phone": "Telefon",
    "Date_of_birth": "Geburtsdatum",
    "gender": "Geschlecht",
    "Sex": "Geschlecht",
    "Identity_Info": "Identitätsinformationen",
    "Required_data": "Erforderliche Daten",
    "Male": "Männlich",
    "Female": "Weiblich",
    "Update": "Aktualisierung",
    "Updating": "Aktualisierung",
    "Identity_Status": "Identitätsstatus",
    "profile_text1": "Bevor Sie die Dokumente hochladen, geben Sie bitte diese Informationen in Ihr Profil ein: Vorname, Nachname, Geburtsdatum, Bundesland / Region, Stadt, Adresse",
    "NOT_VERIFIED": "NICHT ÜBERPRÜFT",
    "VERIFIED": "ÜBERPRÜFT",
    "Waiting_For_Verificaton": "Warten auf Überprüfung",
    "Upload_a_valid_ID": "Hochladen eines gültigen Ausweises",
    "uploading": "Upload",
    "Address_Info": "Adressinfo",
    "Country": "Land",
    "State_Region": "Bundesland / Region",
    "City": "Stadt",
    "Address": "Adresse",
    "Address_Line_2": "Address Line 2",
    "Zip_Code": "Postleitzahl",
    "Address_Status": "Adressstatus",
    "PENDING": "ANHÄNGIG",
    "Upload_valid_address": "Gültige Adresse hochladen",
    "Avatar": "Avatar",
    "Delete_avatar": "Avatar löschen",
    "Upload_profile_picture": "Profilbild hochladen",
    "Hide_my_profile": "Mein Profil ausblenden",
    "Nickname": "Spitzname",
    "Email_notifications": "E-Mail-Benachrichtigungen",
    "Sound_notifications": "Sound-Benachrichtigungen",
    "Language": "Sprache",
    "Delete_account": "Konto löschen",
    "Change_password": "Passwort ändern",
    "Password": "Passwort",
    "old_password": "Altes Passwort",
    "confirm_password": "Passwort bestätigen",
    "security_test1": "Zwei-Faktor-Authentifizierung (2FA)",
    "security_test2": "Zwei-Faktor-Authentifizierung beim Anmelden",
    "recommended": "(Empfohlen)",
    "active": "Aktiv",
    "login_history": "Anmeldeverlauf",
    "ip": "IP",
    "device_os": "Gerät / Betriebssystem",
    "browser": "Browser",
    "date1": "2020-10-23 21:14:27",
    "date2": "102.89.0.223",
    "WebKit_Windows": "WebKit / Windows",
    "Chrome": "Chrome",
    "Nigeria": "Nigeria",
    "Lagos": "Lagos",
    "Active_Sessions": "Aktive Sitzungen",
    "Last_Activity": "Letzte Aktivität",
    "inactive": "Inaktiv",
    // Security
    
    // Trading History
     "live_trading_history": "LIVE-HANDELSGESCHICHTE",
    "show_trading_recommendation": "Handelsempfehlungen anzeigen",
    "Live": "Live",
    "Demo": "Demo",
    "asset": "Asset",
    "open_time": "Öffnungszeit",
    "expiry_time": "Ablaufzeit",
    "open_price": "Offenpreis",
    "closing_price": "Schlusskurs",
    "payout": "Auszahlung (%)",
    "load_more": "Mehr laden",
    "loading": "Laden",
    "investment": "Investition",
    "result_pl": "Ergebnis (P / L)",
    "result": "Ergebnis",
    "position_closed_automatically": "Position automatisch geschlossen",
    "purchase_time": "Kaufzeit",
    "expiration_time": "Ablaufzeit",
    "strike": "Strike",
    "trade_time": "Trade Time",
    "time": "Time",
    "opening_price": "Eröffnungspreis",
    "position_id": "Positions-ID:",
    "coefficient": "Koeffizient",
    "available_balance": "VERFÜGBARES GLEICHGEWICHT",
    "demo": "Demo",
    "close": "SCHLIESSEN",
    "strike_price": "Ausübungspreis",

    // Trading Profile
    "profit": "Gewinn",
    "ID": "ID",
    "trading_profile_text1": "Live-Kontostatistik",
    "UID": "UID",
    "Today": "Heute",
    "Yesterday": "Gestern",
    "All_time": "Alle Zeit",
    "Deals": "Deals",
    "Hedged_turnover": "Absicherter Umsatz",
    "Max_trade_amount": "Maximaler Handelsbetrag",
    "Min_trade_amount": "Minimaler Handelsbetrag",
    "Max_profit_per_deal": "Maximaler Gewinn pro Deal",
    "See_trading_recommendations": "Siehe Handelsempfehlungen",
    "Invest_real_money": "Echtes Geld investieren",
    "Open_live_account": "Live-Konto eröffnen",
    "Get_50%_bonus": "50% Bonus erhalten",
    "on_your_deposit": "auf Ihre Einzahlung",
    "No_Data": "Keine Daten",
    "No_trading_history": "Keine Handelshistorie ]",
    "History_of_live_trades": "Geschichte der Live-Trades",
    "History_of_demo_trades": "Geschichte der Demo-Trades",
    "History_of_Social_Trading": "Geschichte des Social Trading",
    "No_live_trades": "Keine Live-Trades",

    // Trading Folder
    // App
    "Trades": "Trades",
    "Express_Trades": "Express Trades",
    "Opened": "Geöffnet",
    "Closed": "Geschlossen",
    "View_history_of_demo_trades": "Verlauf der Demo-Trades anzeigen",
    "recent_trades": "Letzte Trades",

    // Help Folder

    // Support View
    "Message": "Nachricht",
    "Last_update": "Letzte Aktualisierung",
    "white_tee": "Video-Tutorial",
    "Last_Update": "Letzte Aktualisierung",
    "closed": "geschlossen",
    "Verification": "Überprüfung",
    "Account": "Konto",
    "Home": "Startseite",
    "Messages": "Nachrichten",
    "View_Platform_Guide": "Plattformhandbuch anzeigen",
    "Create_Support_Request": "Neue Supportanfrage erstellen",
    "Video_Tutorial": "Video-Tutorial",

    // credit demo modal
    "credit_demo_text1": "Wechseln Sie zu einem digitalen Handel",
    "credit_demo_text2": "Virtuelles Geld auf Ihrem Demo-Konto",
    "Support_request": "Supportanfrage",
    "Files": "Dateien",
    "Author": "Autor",
    "Dear_Client": "Sehr geehrter Kunde",
    "support_ticket_text": "Mein Name ist Stanislav und ich bin Ihr persönlicher Manager. Benötigen Sie Unterstützung? Haben Sie Fragen zur Handelsplattform? Falls Sie auf Schwierigkeiten stoßen würden Ich empfehle Ihnen gerne, unseren Plattform-Leitfaden zu lesen. Wenn Sie Fragen haben, können Sie sich gerne an unseren Support-Service wenden. Viel Glück beim Handeln und einen schönen Tag!",
    "file": "Datei",
    "Support_Specialist": "Support-Spezialist",
    "Back": "Zurück",
    "Support_request_is_closed": "Support-Anfrage ist geschlossen"
}