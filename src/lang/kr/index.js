import homepage from './homepage';
import auth from './auth';
import dashboard from './dashboard';
import affiliate from './affiliate';

export default {
    home:homepage,
    auth,
    dashboard,
    affiliate
}