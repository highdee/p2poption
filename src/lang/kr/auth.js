export default {
    "to_home_page": "홈페이지로",
    "login": "로그인",
    "not_registered": "아직 등록되지 않음",
    "registration": "등록",
    "email": "이메일",
    "password": "비밀번호",
    "remember_me": "기억하기",
    "forgot_your_password": "비밀번호 분실",
    "verifying": "확인 중",
    "or_login_with": "또는 로그인",
    "facebook":"페이스 북",
    "google": "Google",
    "contacts": "연락처",
    "aml_policy": "AML 및 KYC 정책",
    "payment_policy": "결제 정책",
    "terms_and_conditions": "이용 약관",
    "privacy_policy": "개인 정보 보호 정책",
    "responsibility_disclosure": "책임 공개",
    "copyright": "저작권",

    // 비밀번호 복구
    "password_recovery": "비밀번호 복구",
    "back_to_login": "로그인으로 돌아 가기",
    "restore": "복원",
    "sending": "보내기",

    // 암호를 재설정
    "reset_password": "비밀번호 재설정",
    "password_confirmation": "비밀번호 확인",
    "resetting": "재설정",
    "already_registered": "이미 등록됨",
    "or_signup_with": "또는 가입"
}