export default {
  "banner_text_1": "La plupart",
  "banner_text_2": "offres rentables",
  "banner_text_3": "pour les affiliés",
  "banner_title_login_1": "Profit sharing commission is up to 80%",
  "banner_text_login_1": "Améliorez le statut de votre profil pour augmenter votre commission de partage des bénéfices de 50% à 80% en tant qu'IB!",
  "banner_title_login_2": "Profit sharing commission is up to 80%",
  "banner_text_login_2": "Améliorez le statut de votre profil pour augmenter votre commission de partage des bénéfices de 50% à 80% en tant qu'IB!",
  "banner_title_login_3": "Profit sharing commission is up to 80%",
  "banner_text_login_3": "Améliorez le statut de votre profil pour augmenter votre commission de partage des bénéfices de 50% à 80% en tant qu'IB!",
  "login": "Connexion",
  "register": "Connexion",
  "login_fb": "Facebook",
  "login_g": "Google",
  "login_or": "ou",
  "login_fp": "Se souvenir de moi",
  "login_rem": "Mot de passe oublié",
  "sign_in_btn": "Connexion",
  "authorizing": "Autoriser",
  "not_yet": "Pas encore inscrit",
  "sign_up_alt": "Inscription",
  "toc": "J'ai lu et accepté l'accord d'affiliation",
  "sign_up_btn": "inscription",
  "sign_up_btn_loading": "chargement",
  "sign_up_alt_log": "Déjà inscrit",
  "sign_up_alt_btn": "Connexion",
  "reset_password": "Mot de passe oublié",
  "resetting_password": "Réinitialiser le mot de passe",
  "resetting_password_btn": "Réinitialiser le mot de passe",
  "resetting_password_btn_loading": "Réinitialiser le mot de passe",
  "reset_password_btn": "Réinitialiser le mot de passe",
  "reset_password_btn_loading": "Envoi de l'e-mail",
  "Campaign_name": "Nom de la campagne",
  "Promo_code": "Code promotionnel",
  "Offer_type": "Type d'offre",
  "Cashback": "Remise en argent",
  "text4": "Ne pas utiliser le code promotionnel",

  // affiliate header
  "Visitors": "visiteurs",
  "Reg": "Reg",
  "Commission": "Commission",
  "Balance": "Balance",
  "refferal_link": "lien de référence",

  // App
  "Dashboard": "Tableau de bord",
  "My_affiliate_links": "Mes liens d'affiliation",
  "Profile": "Profil",
  "Logout": "Déconnexion",

  // Login
  "affiliates_header": "Offres les plus rentables pour les affiliés",
  "affiliate_hero_text1": "Partage des bénéfices! Système avancé de partage des bénéfices avec un revenu passif à long terme.",
  "affiliate_hero_text2": "Bonus pour les FTD! Système de bonus supplémentaire avec paiements pour les premiers dépôts des clients.",
  "affiliate_hero_text3": "Paiements hebdomadaires! - Plus le statut de votre profil est élevé, plus vous pouvez recevoir de paiements fréquents.",
  "affiliate_hero_text4": "Commission de chiffre d'affaires net! Vous recevez une commission pour tout pari. Plus le volume de trading est important, plus vous avez de commission.",
  "affiliate_hero_text5": "Concours d'affiliation avec participation gratuite. Obtenez une commission supplémentaire et gagnez de superbes prix pour votre travail.",
  "Sign_in": "Connexion",
  "E-mail": "E-mail",
  "Password": "Mot de passe",
  "Forgot_your_password": "Mot de passe oublié?",
  "Not_yet_registered": "Pas encore inscrit?",
  "Authorizing": "Autorisation",
  "footer_h5": "Profit sharing commission is up to 80%",
  "footer_p1": "Améliorez le statut de votre profil pour augmenter votre commission de participation aux bénéfices de 50% à 80% en tant qu'IB!",
  "footer_p2": "Attirez de nouveaux clients et obtenez des bonus supplémentaires!",
  "footer_p3": "Plus vous attirez de clients avec des FTD, plus le montant du bonus que vous obtenez est élevé!",
  "footer_p4": "Gagnez de l'argent sur le chiffre d'affaires net de vos filleuls!",
  "footer_p5": "Obtenez une commission sur chaque pari de parrainage. Lorsque l'entreprise réalise des bénéfices, vous gagnez également des revenus supplémentaires grâce au partage des commissions.",

  // Register
  "Confirm_Password": "Confirmer le mot de passe",
  "I_accepted": "J'ai lu et accepté le",
  "affiliate_agreement": "contrat d'affiliation",
  "Already_registered": "Déjà inscrit?",
  "Sign_up": "Inscription",
  "Registering": "Inscription",

  // Reset Password
  "Reset_Password": "Réinitialiser le mot de passe",
  "Password_Reset_Link": "Envoyer un lien de réinitialisation du mot de passe",
  "Sending_email": "Envoyer un e-mail",
  "Go_back": "Revenir",
  "New_Password": "Nouveau mot de passe",
  "Enter_new_password": "Entrez le nouveau mot de passe",
  "Re-enter_password": "Ressaisissez le mot de passe",
  "Resseting_Password": "Resseting Password",
  "Smart_link": "Smart link",
  "text1": "Ceci est un lien par défaut créé pour obtenir la meilleure conversion lors de l'enregistrement et des dépôts",
  "text2": "Le lien est généré de manière à atteindre les meilleurs résultats en termes de conversion de trafic. Votre trafic sera redirigé vers les pages du site Web qui assurent le plus de revenus.",

  "Start": "START",
  "bonus": "bonus",
  "min_deposit": "dépôt min.",
  "MORE_CHANCE": "PLUS DE CHANCE",
  "Volume_sharing": "Partage de volume",
  "Turnover_sharing": "Partage de chiffre d'affaires",
  "Deposit_sharing": "Partage de dépôt",
  "Revenue_sharing": "Partage de revenus",
  "CPA": "CPA",
  "text5": "Cashback - partagez une partie de votre commission avec vos filleuls. Le cashback est disponible en option pour les programmes de partage de volume et de partage du chiffre d'affaires. Il est calculé individuellement pour chaque parrainage en fonction du volume net des transactions pour la période de règlement.",
  "Create": "Créer",
  "creating": "créer",
  "Campaigns": "Campagnes",
  "Campaign_Name": "Nom de la campagne",
  "Register_user": "Enregistrer l'utilisateur",
  "CTR": "CTR",
  "RTD": "RTD",
  "Link": "Lien",
  "Delete": "Supprimer",
  "deleting": "suppression",
  "Find": "Rechercher",
  "Yesterday": "Hier",
  "Current_week": "Semaine en cours",
  "Current_month": "Mois en cours",
  "Last_week": "Semaine dernière",
  "Last_month": "Mois dernier",
  "Custom_settings": "Paramètres personnalisés",
  "Date_from": "Date du",
  "Date_to": "Date au",
  "Referral_link": "Lien de parrainage",
  "text6": "Votre compte affilié a été banni, veuillez contacter notre fournisseur de services pour l'activer",
  "Personal_information": "Informations personnelles",
  "Save": "Enregistrer",
  "Saving": "Enregistrer",
  "smart_link": "Liens intelligents",
  "smart_link_text": "Il s'agit d'un lien par défaut créé pour obtenir la meilleure conversion en matière d'inscription et de dépôts",
  "smart_link_paragraph": "Le lien est généré de manière à obtenir les meilleurs résultats en termes de conversion de trafic. Votre trafic sera redirigé vers les pages du site Web qui assurent le plus de revenus",
  "create_new_link": "Créer un nouveau lien d'affiliation",
  "create_btn": "créer",
  "create_btn_loading": "créer",
  "campaign": "Campagne",
  "campaign_name": "Nom de la campagne",
  "offer_type": "Type d'offre",
  "promo_code": "Code promotionnel",
  "register_user": "Enregistrer l'utilisateur",
  "personal_info": "Informations personnelles",
  "first_name": "prénom",
  "last_name": "nom de famille",
  "email": "e-mail",
  "username": "nom d'utilisateur",
  "phone": "nom d'utilisateur",
  "country": "Pays",
  "profile_btn": "enregistrer",
  "profile_btn_loading": "Mise à jour",
  "today": "Aujourd'hui"
  // Recover Password
   
  // profile
}