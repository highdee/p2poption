import VueI18n from 'vue-i18n'
import Vue from 'vue'
import EnIndex from './en/index'
import KrIndex from './kr/index'
import CnIndex from './cn/index'
import JpIndex from './jp/index'
import ItIndex from './it/index'
import EsIndex from './es/index'
import PtIndex from './pt/index'
import ArIndex from './ar/index'
import InIndex from './in/index'
import FrIndex from './fr/index'
import GeIndex from './ge/index'
import HiIndex from './hi/index'
import RmIndex from './rm/index'

Vue.use(VueI18n);
var lang=window.localStorage.getItem('language');
if(!lang){
	window.localStorage.setItem('language', process.env.VUE_APP_DEFAULT_LANG);
}else{
	if(lang == 'undefined'){ 
		window.localStorage.setItem('language', process.env.VUE_APP_DEFAULT_LANG);
	}
}
// Create VueI18n instance with options
const i18n = new VueI18n({
	locale: window.localStorage.getItem('language'), // set locale
	messages:{  
		en : EnIndex,
        kr : KrIndex,
        cn : CnIndex,
        jp : JpIndex,
        it : ItIndex,
        es : EsIndex,
        pt : PtIndex,
		ar: ArIndex,
		in: InIndex,
		fr: FrIndex,
		ge: GeIndex,
        hi : HiIndex,
        rm : RmIndex,
	},
	silentTranslationWarn: true
});

export default i18n;
