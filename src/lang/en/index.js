import homepage from './homepage';
import dashboard from './dashboard';
import auth from './auth';
import affiliate from './affiliate'

export default {
    home: homepage,
    dashboard: dashboard,
    affiliate: affiliate,
    auth,
}
