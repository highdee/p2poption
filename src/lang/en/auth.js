export default  {
    // Login
    "to_home_page": "To home page",
    "login": "Login",
    "not_registered": "Not yet registered",
    "registration": "Registration",
    "email": "Email",
    "password": "Password",
    "remember_me": "Remember Me",
    "forgot_your_password": "Forgot your password",
    "verifying": "Verifying",
    "or_login_with": "Or login with",
    "facebook": "Facebook",
    "google": "Google",
    "contacts": "Contacts",
    "aml_policy": "AML and KYC Policy",
    "payment_policy": "Payment Policy",
    "terms_and_conditions": "Terms and Conditions",
    "privacy_policy": "Privacy Policy",
    "responsibility_disclosure": "Responsibility Disclosure",
    "copyright": "Copyright ",

    // Password recovery
    "password_recovery": "Password Recovery",
    "back_to_login": "Back to Login",
    "restore": "Restore",
    "sending": "Sending",

    // Reset Password
    "reset_password": "Reset Password",
    "password_confirmation": "Password Confirmation",
    "resetting": "Resetting",
    "already_registered":"Already registered",
    "or_signup_with":"Or sign up with"
}