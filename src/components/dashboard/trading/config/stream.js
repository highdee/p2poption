// api/stream.js
import historyProvider from './historyProvider.js'
var trading_data={symbols_and_type:[]};
var global_socket=null;
var activeSymbolTypeData={};


// keep track of subscriptions
var _subs = [];
var emitter=null;
var channelString='';
var symbol='';
var crypto_symbols=[];
var forex_symbols=[];
var connected_count = 0;
var USERID = 0;
// emitter.on('subscribeAllSymbols',function(data){
	
// });

export default {
	// CUSTOM METHOD
	setUserId(id){
		USERID  = id;
	},
	subscribeBars: function(symbolInfo, resolution, updateCb, uid, resetCache, emitter_ev) { 

		var symbolname=symbolInfo.name.replace('/','');
		channelString = symbolname.toLowerCase();
		symbol=symbolInfo; 
		activeSymbolTypeData=trading_data['symbols_and_type'][symbolInfo.name];

		emitter=emitter_ev;
	
		var newSub = {
			channelString,
			uid,
			resolution,
			symbolInfo,
			lastBar:  null,
			listener: updateCb,
		}
		_subs.push(newSub);
		resetCache();
 
		if(global_socket == null){
			subscribeToChannel();
		}
		
	},
	unsubscribeBars: function(uid){
		// unSubscribe(global_socket,[channelString]);
		// var subIndex = _subs.findIndex(e => e.uid === uid)
		// if (subIndex === -1) {
			console.log("Unsubscribed");
		// 	return
		// }
		// var sub = _subs[subIndex]
		// socket.emit('SubRemove', {subs: [sub.channelString]})
		// _subs.splice(subIndex, 1)
	},
	// CUSTOM METHOD
	setTradingData(data){
        trading_data=data;
    }
} 
 

function subscribeToChannel(){
	global_socket = {};
	var socket=new WebSocket("wss://server.optionp2p.io:3000/"+USERID);
	// var socket=new WebSocket("ws://127.0.0.1:3001/"+USERID);

	socket.onopen = function(){
		global_socket = socket;
		emitter.emit('global_socket_opened',global_socket);

		console.log('Option p2p socket is open');
	}

	socket.onclose = function(){
		if(connected_count < 15){
			console.log('reconnecting....')
			setTimeout(()=>{
				subscribeToChannel(symbol);
				connected_count+=1;
			},1000);
		}
	}
	subscribeToChannelMessages(socket);
}
  
function subscribeToChannelMessages(socket){
	socket.onmessage=function(e){ 
		var _data=JSON.parse(e.data);
		// console.log(_data);

		if(!_data.currency){
			emitter.emit('socket_message',_data);
			return false;
		}
		
		if(_data.open == 'n/a' || _data.close == 'n/a'){
			return false;
		}
	
		const data =  {
            time: getTimeEndInMilli(_data.timestamp),
            open: parseFloat(_data.open),
            high: parseFloat(_data.high),
            low: parseFloat(_data.low),
            close: parseFloat(_data.close),
            ask: parseFloat(_data.ask),
            bid: parseFloat(_data.bid), 
			// volume: parseFloat(_data.ask),
			current_time:_data.timestamp,
			symbol:_data.currency
        };

		
		//EMIT THE DATA TO THE VUE COMPONENT THAT CREATED THIS SOCKET/EVENTS OBJECT
		if(emitter){
			emitter.emit('trade_data',data);
		} 
		
		const sub = _subs.find(e => e.channelString === channelString);
		

		if (sub){ 

			
			// disregard the initial catchup snapshot of trades for already closed candles
			//Meaning: if this new data has a time that is lesser than the chart lastbar then don't update the chart
			if (sub.lastBar && data.time < (sub.lastBar.time / 1000)) {
				return
			}  
			var _lastBar = updateBar(data, sub);
			if(symbol.name.replace('/','') == data.symbol){ 
				// console.log(data);
				// 	// send the most recent bar back to TV's realtimeUpdate callback
				sub.listener(_lastBar)
				// data.time = convertToNextMinuteTime(data.time);
				// sub.listener(data);
				// 	// update our own record of lastBar
				sub.lastBar = _lastBar

			}
		}
	
	}
	
}
// Take a single trade, and subscription record, return updated bar
function updateBar(data, sub) {
	// var lastBar = JSON.parse(JSON.stringify(sub.lastBar));
	let resolution = sub.resolution
	if (resolution.includes('D')) {
		// 1 day in minutes === 1440
		resolution = 1440
	} else if (resolution.includes('W')) {
		// 1 week in minutes === 10080
		resolution = 10080
	}
	var coeff = resolution * 60
	// console.log({coeff}) 
	var _lastBar={}
  
		// create a new candle, use last close as open **PERSONAL CHOICE**
		_lastBar = {
			time: data.time,
			open: data.open,
			high: data.high,
			low: data.low,
			close: data.close,
			volume: data.volume
		} 
	return _lastBar
}
function getTimeEndInMilli(time){
	var date = new Date(time);
	var timeEnd= new Date(date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate() +' '+date.getHours()+':'+date.getMinutes()+':59');
	return timeEnd.getTime()
} 