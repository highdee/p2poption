import axios  from 'axios';
var history={};
var trading_data={symbols_and_type:[]};


export default {
    history:history,
    getBars: function(symbolInfo, resolution, from, to, first, limit) {
        var bars=[];

        // var api = "http://127.0.0.1:3000/rates/"+symbolInfo.name.replace('/','');
        var api = "https://server.optionp2p.io:3000/rates/"+symbolInfo.name.replace('/','');
        
        return new Promise((resolve,reject)=>{
            axios.get(api)
            .then((data)=>{
                console.log(data);
                // resolve([]);
                bars=processData(data.data);
                
                if(first && bars.length > 0){
                    var lastbar=bars[bars.length - 1];
                    history[symbolInfo.name]={lastBar: lastbar};
                }
                resolve(bars);
            })
            .catch((error)=>{
                console.log(error);
                reject(error);
            })
        });
    },
    setTradingData(data){
        trading_data=data;
    }
} 
function processData(data){
    return data.map((d)=>{
        return {
            time: getTimeEndInMilli(d.timestamp),
            open: parseFloat(d.open),
            high: parseFloat(d.high),
            low: parseFloat(d.low),
            close: parseFloat(d.close),
            ask: parseFloat(d.ask),
            bid: parseFloat(d.bid),
            // volume: parseFloat(d.ask),
        }
    });
}
function getTimeEndInMilli(time){
    var date = new Date(time);
	var timeEnd= new Date(date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate() +' '+date.getHours()+':'+date.getMinutes()+':59');
	return timeEnd.getTime()
}