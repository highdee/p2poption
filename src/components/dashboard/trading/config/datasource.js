import historyProvider from './historyProvider';
import stream from './stream';

// const supportedResolutions = ["1", "3", "5", "15", "30", "60", "120", "240", "D"]
const supportedResolutions = ["1",]
var assets_bars={};
const config = {
    supported_resolutions: supportedResolutions
};
var current_symbol="";
var emitter=null;
var trading_data={symbols_and_type:[]};
var symbol_has_fetch_bar_before = {};

export default {
    feed_emitter:emitter,
    onReady: cb => {
        console.log('=====onReady running')
        setTimeout(() => cb(config), 0)
    },
    searchSymbols: (userInput, exchange, symbolType, onResultReadyCallback) => {
        console.log('====Search Symbols running',userInput,exchange,onResultReadyCallback);
        onResultReadyCallback([{name:'BTC/USDT'}]);
    },
    resolveSymbol: (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) => {
        // expects a symbolInfo object in response
        console.log('======resolveSymbol running');
        console.log(symbolName);
        // console.log('resolveSymbol:',{symbolName})
        var split_data = symbolName.split(/[:/]/)
        // console.log({split_data})
        var symbol_stub = {
            name: symbolName,
            description: '',
            type: 'forex',
            session: '24x7',
            timezone: 'Etc/UTC',
            ticker: symbolName,
            exchange: split_data[0],
            minmov: 1,
            // pricescale: 100000000,
            pricescale: 1000,
            has_intraday: true,
            intraday_multipliers: ['1', '60'],
            supported_resolution:  supportedResolutions,
            volume_precision: 8,
            data_status: 'streaming',
        }

        // if (split_data[2].match(/USD|EUR|JPY|AUD|GBP|KRW|CNY/)) {
        //     symbol_stub.pricescale = 100
        // }
        setTimeout(function() {
            onSymbolResolvedCallback(symbol_stub)
            console.log('Resolving that symbol....', symbol_stub)
        }, 0)
        
        
        // onResolveErrorCallback('Not feeling it today')
    },
    getBars: function(symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) { 
        console.log('=====getBars runnning',firstDataRequest);
        current_symbol=symbolInfo.name.replace('/','');

        if(!assets_bars[current_symbol]){
            assets_bars[current_symbol]=[];
        }
        
        if(!firstDataRequest){
            onHistoryCallback([], {noData : true}); 
        }
 
        
        if(assets_bars[current_symbol].length > 0 && symbol_has_fetch_bar_before[current_symbol]){
            onHistoryCallback(assets_bars[current_symbol], {noData : false});
            return false;
        }
 
        historyProvider.getBars(symbolInfo, resolution, from, to, firstDataRequest)
        .then(bars => {  
            assets_bars[current_symbol]=[...bars, ...assets_bars[current_symbol]];

            if(firstDataRequest){
                symbol_has_fetch_bar_before[current_symbol] = true;
                onHistoryCallback(assets_bars, {noData : false});
            }else{
                onHistoryCallback([], {noData: true})
            }
            emitter.emit('bars_ready','');
        }).catch(err => {
            console.log({err})
            onErrorCallback(err)
        })

    },
    subscribeBars: (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) => {
        console.log('=====subscribeBars runnning');
        
        stream.subscribeBars(symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback,emitter);
        
    },
    unsubscribeBars: subscriberUID => {
        console.log('=====unsubscribeBars running');
        // emitter.emit('trade_loading',true);
        // stream.unsubscribeBars(subscriberUID);
    },
    calculateHistoryDepth: (resolution, resolutionBack, intervalBack) => {
        //optional
        // console.log('=====calculateHistoryDepth running',resolution)
        // while optional, this makes sure we request 24 hours of minute data at a time
        // CryptoCompare's minute data endpoint will throw an error if we request data beyond 7 days in the past, and return no data
        return resolution < 60 ? {resolutionBack: 'D', intervalBack: '1'} : undefined
    },
    getMarks: (symbolInfo, startDate, endDate, onDataCallback, resolution) => {
        //optional
        // console.log('=====getMarks running')
    },
    getTimeScaleMarks: (symbolInfo, startDate, endDate, onDataCallback, resolution) => {
        //optional
        // console.log('=====getTimeScaleMarks running')
    },
    getServerTime: cb => {
        // console.log('=====getServerTime running');
    },
    // CUSTOM METHOD
    setEmitter:(emitta)=>{
        emitter=emitta;
        
        emitter.on('trade_data',(trade_data)=>{
            
            var _data=trade_data;
            assets_bars[_data.symbol] = assets_bars[_data.symbol] ?? [];
            assets_bars[_data.symbol].push(_data);
            
            // var current_bar_length=assets_bars[_data.symbol].length;
            // var from=assets_bars[_data.symbol][current_bar_length - 100];
            // emitter.emit('set_range',{from:from , to:_data });
        });
    },
    // CUSTOM METHOD
    setUserId(id){
        stream.setUserId(id);
    },
    // CUSTOM METHOD
    setTradingData(field,value){
        trading_data[field]=value;
        historyProvider.setTradingData(trading_data);
        stream.setTradingData(trading_data);
    }
}