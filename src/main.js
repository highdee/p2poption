import Vue from 'vue'
import App from './App.vue'

// import Vuex from 'vuex'
import Routes from './route/index'
import store from './store/index'

import Toasted from 'vue-toasted';
import notification from './components/layout/notification'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import loader from './components/layout/loader'
import GoogleAuth from '@/config/google.js'
import i18n from './lang/index'
import VueCountryCode from "vue-country-code";
import vSelect from 'vue-select'
import MoneyFormat from 'vue-money-format'
import money from './components/dashboard/layout/money';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
 
Vue.use(VueToast);
Vue.component('v-select', vSelect);
Vue.component('money-format', MoneyFormat);
Vue.component('Money', money);
Vue.use(VueCountryCode);

// let instance = Vue.$toast.open('You did it!');

const gauthOption = {
  clientId: '405489142264-8aht7j2jbv3dip6tj9slc8nq1bljp3uf.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}
Vue.use(GoogleAuth, gauthOption);
Vue.config.productionTip = false
Vue.component('notification', notification);
Vue.component('loader', loader);
Vue.use(Toasted);
Vue.use(Notifications, { velocity });

new Vue({
  router: Routes,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
