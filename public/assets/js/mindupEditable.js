var table_data = [
    { fname: "Code", lname: "With Mark", email: "mark@codewithmark.com" },
    { fname: "Mary", lname: "Moe", email: "mary@gmail.com" },
    { fname: "John", lname: "Doe", email: "john@yahoo.com" },
    { fname: "Julie", lname: "Dooley", email: "julie@gmail.com" },
]

var random_id = function () {
    var id_num = Math.random().toString(9).substr(2, 3);
    var id_str = Math.random().toString(36).substr(2);

    return id_num + id_str;
}
//--->create data table > start
var tbl = '';
tbl += '<table class="table table-hover">'

//--->create table header > start
tbl += '<thead>';
tbl += '<tr>';
tbl += '<th>First Name</th>';
tbl += '<th>Last Name</th>';
tbl += '<th>Email</th>';
tbl += '<th>Options</th>';
tbl += '</tr>';
tbl += '</thead>';
//--->create table header > end


//--->create table body > start
tbl += '<tbody>';

//--->create table body rows > start
$.each(table_data, function (index, val) {
    //you can replace with your database row id
    var row_id = random_id();

    //loop through ajax row data
    tbl += '<tr row_id="' + row_id + '">';
    tbl += '<td ><div class="row_data" edit_type="click" col_name="fname">' + val['fname'] + '</div></td>';
    tbl += '<td ><div class="row_data" edit_type="click" col_name="lname">' + val['lname'] + '</div></td>';
    tbl += '<td ><div class="row_data" edit_type="click" col_name="email">' + val['email'] + '</div></td>';

    //--->edit options > start
    tbl += '<td>';

    tbl += '<span class="btn_edit" > <a href="#" class="btn btn-link " row_id="' + row_id + '" > Edit</a> </span>';

    //only show this button if edit button is clicked
    tbl += '<span class="btn_save"> <a href="#" class="btn btn-link"  row_id="' + row_id + '"> Save</a> | </span>';
    tbl += '<span class="btn_cancel"> <a href="#" class="btn btn-link" row_id="' + row_id + '"> Cancel</a> | </span>';

    tbl += '</td>';
    //--->edit options > end

    tbl += '</tr>';
});

//--->create table body rows > end

tbl += '</tbody>';
//--->create table body > end

tbl += '</table>'
//--->create data table > end


//out put table data


// $(document).find

// $('#tbl_user_data').html("<h3>Hello world</h3>");

// console.log($(document).find('#tbl_user_data'))

// $(document).find('.btn_save').hide();

// $(document).find('.btn_cancel').hide();