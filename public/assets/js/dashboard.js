// function closeNav() {
//     const sidebar_container = document.querySelector(".sidebar")
//     const hamburgger = document.querySelector("#menu-toggle")
//     sidebar_container.classList.remove('d-block')
//     hamburgger.classList.add('bx-menu')
// }
// (function navCloser() {
//     const nav_list = document.querySelectorAll('.side-tooltip')
//     nav_list.forEach(nav => {
//         // nav.addEventListener("click", () => closeNav());
//         console.log(nav)
//     })
// })();

$(document).ready(function () {
    // var url = window.location.href
    // var current = url.split('/')
    // var last = current[current.length - 1];
    // if(last == 'trade-room'){
    //     var doc = document.getElementById('dashboard');
    //     doc.classList.add("trade_landscape");
    //     console.log(doc)
    //     viewport = document.querySelector("meta[name=viewport]");
    //     viewport.setAttribute('content', 'width=device-width, initial-scale=0.5, maximum-scale=1.0, user-scalable=no');
    //     console.log(viewport)
    // }

    // screen.orientation.lock('landscape');
    $("#navs-toggler").click(function (e) {
        e.preventDefault();
        $(".public-layout").toggleClass("toggled");
        $("#menu-icon").toggleClass("bx-menu");
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.rm-title').on('click', () => {
        $('.side-tooltip p').toggleClass('remove-p')
        $('.side-tooltip').toggleClass('rm-padding')
        $('.rm-title i').toggleClass('fas fa-long-arrow-alt-right fas fa-long-arrow-alt-left')
    })
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $(".sidebar").toggleClass("d-block");
        $("#menu-toggle").toggleClass('bx-menu')
        // $(".sm-nav-icon").toggleClass('nav-mid')
        $('#tradHistory-item').removeClass('show');
        $('#tradHistory').removeClass('show');
    });

    $("#chart-toggle").click(function (e) {
        e.preventDefault();
        $(".sm-trade-sidebar").toggleClass("chart-toggled");
        $(".sm-chart-toggler").toggleClass('chart-mid')
    });

    $('.tm-title').on('click', () => {
        $('.trade-tooltip p').toggleClass('remove-p')
        $('.trade-tooltip').toggleClass('rm-padding')
        $('.tm-title i').toggleClass('fas fa-long-arrow-alt-right fas fa-long-arrow-alt-left')
    })
    $('.demo').click(function () {
        if ($("#profile-dropdown").hasClass('show-profile-dropdown')) {
            $("#profile-dropdown").removeClass('show-profile-dropdown')
        }
    })
    $('#main-page-container').click(function (e) {

        if ($(".sm-trade-sidebar").hasClass('chart-toggled')) {
            $(".sm-trade-sidebar").removeClass("chart-toggled");
            $(".sm-chart-toggler").removeClass('chart-mid')
        }

        if ($(".sm-sidebar").hasClass('toggled')) {
            $(".sm-sidebar").removeClass("toggled");
            $(".sm-nav-icon").removeClass('nav-mid')
        }
        if ($("#profile-dropdown").hasClass('show-profile-dropdown')) {
            $("#profile-dropdown").removeClass('show-profile-dropdown')
        }

    })
    $('.close-tab').click(function (e) {
        $('#main-page-container').click()
    })
    $('.remove-collapse').click(() => {
        $('#chat').removeClass('show');
        $('#tradHistory').removeClass('show');
    })
    $('.remove-tradHistory-item').click(() => {
        $('#tradHistory-item').removeClass('show');
    })
    $('#profile-toggle').click(() => {
        $('#profile-dropdown').toggleClass('show-profile-dropdown');
        // position: absolute; transform: translate3d(811px, 58px, 0px); top: 0px; left: 0px; will-change: transform;
        $('#profile-dropdown').css({ 'position': 'absolute', 'transform': 'translate3d(647px, 58px, 0px)', 'top': '7px', 'left': '0px', 'will-change': 'transform' })
    })
    $('#sm-liveDemoBtn1').click(() => {
        $('#lg-liveDemoDrop').toggleClass('show-profile-dropdown');
        // position: absolute; transform: translate3d(811px, 58px, 0px); top: 0px; left: 0px; will-change: transform;

        $('#lg-liveDemoDrop').css({ 'border': '10px solid red', 'position': 'absolute', 'transform': 'translate3d(775px, 58px, 0px)', 'top': '7px', 'left': '0px', 'will-change': 'transform' })
    })
});